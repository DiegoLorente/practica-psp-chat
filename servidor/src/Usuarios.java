/**
 * Created by Diego Lorente on 09/03/2016.
 */
public class Usuarios {
    private String usuario;
    private String clave;


    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return clave;
    }

    public void setContrasena(String clave) {
        this.clave = clave;
    }

    public String toString(){
        return usuario;
    }
}
