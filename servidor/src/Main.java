import java.io.IOException;

/**
 * Created by Diego Lorente on 09/03/2016.
 */
public class Main {

    public static void main(String args[]) {

        int puerto = 5555;

        Servidor servidor = new Servidor(puerto);
        Cliente cliente = null;

        try {
            servidor.conectar();
            while (servidor.comprobarConexion()) {
                cliente = new Cliente(servidor.aceptarConexion(), servidor);
                cliente.start();
                System.out.println("Nueva conexion aceptada");
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
