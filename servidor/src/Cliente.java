import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by Diego Lorente on 09/03/2016.
 */
public class Cliente extends Thread{
    private Socket socket;
    private PrintWriter salida;
    private BufferedReader entrada;
    private Servidor servidor;
    private String nick;
    private String ip;


    public Cliente(Socket socket, Servidor servidor) throws IOException {
        this.socket = socket;
        this.servidor = servidor;

        salida = new PrintWriter(socket.getOutputStream(), true);
        entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    public PrintWriter getSalida() {
        return salida;
    }

    public void cambiarNick(String nick) {
        this.nick = nick;
    }

    public String getNick() {
        return nick;
    }
    @Override
    public void run() {
        boolean encontrado = false;


        ip = socket.getInetAddress().getHostName();
        for (int i = 0; i < servidor.getListaIps().size(); i++) {
            if (servidor.getListaIps().get(i).equals(ip)) {
                encontrado = true;
            }
        }
        servidor.guardarIP(this);
        if (encontrado) {
            salida.println("/server ya hay un cliente desde esta ip");
            salida.println("/quit");
        } else {


            salida.println("/server Introduce tu nick y una clave y pulsa registrarte");
            try {
                String mensaje = entrada.readLine();
                String[] operaaciones = mensaje.split(" ");
                String nick = operaaciones[0];
                cambiarNick(nick);
                servidor.guardarCliente(nick, operaaciones[1], this);
                do {
                    if (servidor.isRegistrado() == false) {
                        mensaje = entrada.readLine();
                        operaaciones = mensaje.split(" ");
                        nick = operaaciones[0];
                        cambiarNick(nick);
                        servidor.guardarCliente(nick, operaaciones[1], this);
                    }
                } while (servidor.isRegistrado() == false);
                servidor.nuevoCliente(this);
                salida.println("/server Bienvenido " + nick);
                salida.println("/sobrenombre " + nick);
                salida.println("/server escribe /quit para cerrar el programa");
                salida.println("/server /private seguido del usuario con el que quieres hablar");
                salida.println("/server y /bloquear seguido del nick para bloquear al usuario");

                if (servidor.isRepetido() == true) {
                    salida.println("/server este usuario ya esta logeado, prueba con otro");
                    salida.println("/quit");
                    socket.close();
                }
                servidor.enviarNicks(this);

                String linea = null;
                while ((linea = entrada.readLine()) != null) {

                    if (linea.equals("/quit")) {
                        socket.close();
                        servidor.eliminarCliente(this);
                        break;
                    }

                    if (linea.startsWith("/bloquear")) {
                        String[] comandos = linea.split(" ");
                        System.out.print(comandos[1] + " bloqueado");
                        salida.println("/bloquear " + comandos[1]);
                    } else if (linea.startsWith("/private")) {
                        String[] comandos = linea.split(" ");
                        int a = comandos[0].length();
                        int b = nick.length();
                        int c = comandos[1].length();
                        int d = a + b + c - 2;
                        String indice = linea.substring(d);
                        System.out.println("/private " + nick + " " + comandos[1] + ": " + indice);
                        servidor.enviarATodos("/private " + nick + " " + comandos[1] + ": " + indice);
                    } else {
                        servidor.enviarATodos("/users " + nick + " " + linea);
                    }

                }

            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }


    public String getIp() {
        return ip;
    }
}
