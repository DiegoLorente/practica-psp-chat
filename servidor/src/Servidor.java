import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by Diego Lorente on 09/03/2016.
 */
public class Servidor {
    private int puerto;
    private ServerSocket socket;

    private ArrayList<Cliente> listaClientes;
    private ArrayList<String> listaBloqueados;
    private ArrayList<String> listaIps;
    private ArrayList<Usuarios> listaUsuariosConectados;
    private boolean usuarioRegistrado;
    private boolean usuarioRepetido;

    public Servidor(int puerto) {
        this.puerto = puerto;
        listaClientes = new ArrayList<>();
        listaBloqueados = new ArrayList<>();
        listaIps = new ArrayList<>();
        listaUsuariosConectados = new ArrayList<>();
    }

    public void nuevoCliente(Cliente cliente) {
        for (int i=0; i<listaClientes.size(); i++){
            if(listaClientes.get(i).getNick().equals(cliente.getNick())){
                return;
            }
        }
        listaClientes.add(cliente);
    }

    public void eliminarCliente(Cliente cliente) {
        listaClientes.remove(cliente);
        enviarNicks(cliente);
    }


    public void enviarNicks(Cliente thiscliente) {

        for (Cliente cliente : listaClientes) {
            cliente.getSalida().println(obtenerNicks());
        }
        thiscliente.getSalida().println(obtenerNicks());
    }

    public String obtenerNicks() {

        String nicks = "/nicks,";
        for (Cliente cliente : listaClientes)
            nicks += cliente.getNick() + ",";

        return nicks;
    }

    public int getNumeroClientes() {
        return listaClientes.size();
    }

    public boolean comprobarConexion() {
        return !socket.isClosed();
    }

    public void conectar() throws IOException {
        socket = new ServerSocket(puerto);
    }

    public void desconectar() throws IOException {
        socket.close();
    }

    public Socket aceptarConexion() throws IOException {
        return socket.accept();
    }

    public boolean comprobarCliente(Cliente cliente){
        boolean repetido = false;
        for(int i=0; i>listaIps.size(); i++){
            if (listaIps.get(i).equals(socket.getInetAddress().getHostName())) {
                System.out.println(listaIps.get(i));
                System.out.println(cliente.getIp());
                repetido=true;
            }
        }
        return repetido;
    }

    public void guardarIP(Cliente cliente){
        listaIps.add(cliente.getIp());
    }

    public void guardarCliente(String nick, String password, Cliente cliente){
        Usuarios usuario = new Usuarios();
        if(listaUsuariosConectados.size()==0){
            usuario.setUsuario(nick);
            usuario.setContrasena(password);
            listaUsuariosConectados.add(usuario);
            cliente.getSalida().println("/server Usuario registrado");
            usuarioRegistrado=true;
            usuarioRepetido=false;
            return;
        }
        for(int i=0; i<listaUsuariosConectados.size(); i++) {
            if (listaUsuariosConectados.get(i).getUsuario().equals(nick)) {
                if (listaUsuariosConectados.get(i).getContrasena().equals(password)) {
                    cliente.getSalida().println("/server Legeado completo");
                    usuarioRegistrado = true;
                    usuarioRepetido=true;
                    return;
                } else {
                    cliente.getSalida().println("/server clave incorrecta");
                    usuarioRegistrado = false;
                    return;
                }
            }
        }
        usuario.setUsuario(nick);
        usuario.setContrasena(password);
        listaUsuariosConectados.add(usuario);
        cliente.getSalida().println("/server Nuevo usuario registrado, enhorabuena");
        usuarioRegistrado=true;
        usuarioRepetido=false;
        return;

    }

    public boolean isRegistrado() {
        return usuarioRegistrado;
    }

    public boolean isRepetido() {
        return usuarioRepetido;
    }

    public ArrayList<String> getListaIps() {
        return listaIps;
    }

    public void enviarATodos(String mensaje) {
        String[] comandos = mensaje.split(" ");

        for (Cliente cliente : listaClientes){
            cliente.getSalida().println(mensaje);
        }

    }
}
