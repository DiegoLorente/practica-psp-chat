import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created by Diego Lorente on 09/03/2016.
 */
public class Conectar extends JDialog{
    private JPanel panel1;
    private JTextField tfHost;
    private JButton btConectar;
    private JButton btCancelar;

    private String host;
    private Constantes.Action accion;

    public Conectar(){
        setContentPane(panel1);
        pack();
        setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        setModal(true);
        setLocationRelativeTo(null);

        btConectar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                aceptar();
            }
        });

        btCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelar();
            }
        });

    }

    private void aceptar(){
        accion = Constantes.Action.ACEPTAR;

        host = tfHost.getText();
        setVisible(false);
    }

    private void cancelar(){
        accion = Constantes.Action.CANCELAR;
        setVisible(false);
    }

    public String getHost() {
        return host;
    }

    public Constantes.Action mostrarDialogo(){
        setVisible(true);
        return accion;
    }
}
