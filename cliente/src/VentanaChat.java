import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * Created by Diego Lorente on 09/03/2016.
 */
public class VentanaChat {
    private JPanel panel1;
    private JTextField tfMensaje;
    private JList listaUsuarios;
    private JLabel lbEstado;
    private JTextArea textArea1;
    private JList jlUsuarios;

    private DefaultListModel modeloListaUsuarios;
    private Socket socket;
    private PrintWriter salida;
    private BufferedReader entrada;
    private boolean usuarioConectado;
    private static final int PUERTO = 5555;
    private ArrayList<String> listaBloqueados;
    private String nick;

    public VentanaChat() {
        tfMensaje.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    enviarMensaje();
            }
        });

        inicializar();
    }

    public JMenuBar getMenuBar() {

        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Chat");
        menuBar.add(menu);
        JMenuItem menuItemConectar = new JMenuItem("Conectar");
        menuItemConectar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                conectar();
            }
        });
        menu.add(menuItemConectar);
       JMenuItem menuItemDesconectar = new JMenuItem("Desconectar");
        menuItemDesconectar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                desconectar();
            }
        });
        menu.add(menuItemDesconectar);
        return menuBar;
    }

    private void inicializar() {
        modeloListaUsuarios = new DefaultListModel<>();
        jlUsuarios.setModel(modeloListaUsuarios);
        listaBloqueados = new ArrayList<String>();

        tfMensaje.requestFocus();
    }

    private void conectar() {

        final Conectar conecta = new Conectar();

        if (conecta.mostrarDialogo() == Constantes.Action.CANCELAR) {
            return;
        }

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                String host = conecta.getHost();
                conectarServidor(host);
            }
        });
    }

    private void conectarServidor(String servidor) {

        try {
            conectar();
            socket = new Socket(servidor, PUERTO);
            salida = new PrintWriter(socket.getOutputStream(), true);
            entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            usuarioConectado = true;
            lbEstado.setText("servicio correcto");

            Thread hiloRecibir = new Thread(new Runnable() {
                public void run() {
                    while (usuarioConectado) {
                        try {
                            if (socket.isClosed()) {
                                usuarioConectado = false;
                                break;
                            }
                            String mensaje = entrada.readLine();
                            if (mensaje == null)
                                continue;

                            int index = 0;
                            if (mensaje.startsWith("/server")) {
                                index = mensaje.indexOf(" ");
                                textArea1.append("" + mensaje.substring(index + 1) + "\n");
                            } else if (mensaje.startsWith("/users")) {
                                index = mensaje.indexOf(" ", 7);
                                String nick = mensaje.substring(7, index);
                                for(int x=0; x<listaBloqueados.size(); x++){
                                    if(nick.equals(listaBloqueados.get(x))){
                                        textArea1.append("El usuario que ha escrito est� ignorado\n");
                                        break;
                                    }
                                }
                                textArea1.append(" " + nick + ": ");
                                textArea1.append(mensaje.substring(index + 1) + "\n");
                            } else if (mensaje.startsWith("/nicks")) {
                                String[] nicks = mensaje.split(",");
                                modeloListaUsuarios.clear();
                                for (int i = 1; i < nicks.length; i++) {
                                    modeloListaUsuarios.addElement(nicks[i]);
                                }
                            }  else if(mensaje.startsWith("/bloquear")) {
                                String[] palabras = mensaje.split(" ");
                                String usuarioBloqueado = palabras[1];
                                for (int i = 0; i < modeloListaUsuarios.size(); i++) {
                                    if (modeloListaUsuarios.get(i).equals(usuarioBloqueado)) {
                                        textArea1.append(usuarioBloqueado+" bloqueado\n");
                                        modeloListaUsuarios.set(i, modeloListaUsuarios.get(i) + " -> Bloqueado");
                                        listaBloqueados.add(usuarioBloqueado);
                                    }
                                }
                            } else if(mensaje.startsWith("/sobrenombre")) {
                                String[] palabras = mensaje.split(" ");
                                nick = palabras[1];
                            } else if (mensaje.startsWith("/private")){
                                String[] palabras = mensaje.split(" ");
                                String mensajeRecibido = palabras[2];
                                mensajeRecibido = mensajeRecibido.substring(0, mensajeRecibido.length() - 1);
                                if (listaBloqueados.contains(palabras[1])) {
                                    textArea1.append("El usuario " + palabras[1] + " te ha escrito un mensaje privado pero est� ignorado\n");
                                }else {

                                    if (mensajeRecibido.equals(nick)){
                                        index = mensaje.indexOf(":");
                                        textArea1.append(nick + " mensaje " + palabras[1] + ": \n");
                                        textArea1.append("\t"+mensaje.substring(index+1));
                                    } else if (palabras[1].equals(nick)){
                                        textArea1.append("Mensaje enviado \n");
                                    }
                                }

                            } else if(mensaje.startsWith("/quit")){
                                desconectar();
                            }
                        } catch (SocketException se) {
                            desconectar();
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                        }
                    }
                }
            });
            hiloRecibir.start();

        } catch (UnknownHostException uhe) {
            uhe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private void enviarMensaje() {

        String mensaje = tfMensaje.getText();
        salida.println(mensaje);

        tfMensaje.setText("");
    }

    private void desconectar() {
        try {
            salida.println("/quit");
            textArea1.append("Has sido desconectado");
            usuarioConectado = false;
           lbEstado.setText("Servicio desconectado");
            socket.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private void salir() {
        System.exit(0);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Chat");
        VentanaChat ventanaChat = new VentanaChat();
        frame.setJMenuBar(ventanaChat.getMenuBar());
        frame.setContentPane(ventanaChat.panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setSize(600, 600);
    }
}
