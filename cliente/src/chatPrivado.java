import javax.swing.*;

/**
 * Created by Diego Lorente on 09/03/2016.
 */
public class chatPrivado {
    public JPanel panel1;
    public JTextArea taChatPrivado;

    public chatPrivado() {
        JFrame frame = new JFrame("Chat Privado");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public JPanel getPanel1() {
        return panel1;
    }

    public void setPanel1(JPanel panel1) {
        this.panel1 = panel1;
    }

    public JTextArea getTaPrivados() {
        return taChatPrivado;
    }

    public void setTaPrivados(JTextArea taPrivados) {
        this.taChatPrivado = taPrivados;
    }
}
